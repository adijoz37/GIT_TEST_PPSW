#include <LPC21xx.h>
#include "keyboard.h"

#define S0_bm (1<<4)
#define S1_bm (1<<6)
#define S2_bm (1<<5)
#define S3_bm (1<<7)

void Delay(int iMilisekunda)
{
    int i;
    for(i=0; i<5500*iMilisekunda; i++){}
}



enum KeyboardState eKeyboardRead(void)
{
    if (!(IO0PIN & S0_bm))
    {
        return BUTTON_0;
    }
    else if (!(IO0PIN & S1_bm))
    {
        return BUTTON_1;
    }
    else if (!(IO0PIN & S2_bm))
    {
        return BUTTON_2;
    }
    else if (!(IO0PIN & S3_bm))
    {
        return BUTTON_3;
    }
    else
    {
        return RELEASED;
    }
    
}

void KeyboardInit(void)
{
    IO0DIR &= ~(S0_bm | S1_bm | S2_bm | S3_bm);
}
