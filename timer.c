#include <LPC21xx.H>
#include "timer.h"

#define mTIMER0_ENABLE (1<<0)
#define mTIMER0_RESET (1<<1)
#define mTIMER0_MR0_INTERRUPT_ENABLE (1<<0)
#define mTIMER0_MR0_RESET_TC (1<<1)
#define mTIMER0_MR0_INTERRUPT_FLAG (1<<0)

void InitTimer0(void){
  T0TCR = mTIMER0_ENABLE; // CTR ENABLE
}


void WaitOnTimer0(unsigned int uiTime){
	T0TCR |= mTIMER0_RESET; // CTR RESET BIT = 1
	T0TCR &=~ mTIMER0_RESET; // CTR RESET = 0 --> wylacz (zaprzestaj) resetu (przestaw bit)
  while( T0TC<(uiTime*15) ){}; // 1. Counter 32 bit zapelnia sie w trakcei zliczania 
														// 2. gdy counter jest mniejsze od wartosci, czekaj (wait)
}

void InitTimer0Match0(unsigned int iDelayTime){
	T0MR0 = iDelayTime*15; // MATCH REGISTER (wartosc)
	T0MCR |= mTIMER0_MR0_INTERRUPT_ENABLE | mTIMER0_MR0_RESET_TC; // po udanym porownaniu, zglos przerwanie i zresertuj LICZNIK timera
	T0TCR |= mTIMER0_RESET; // zresetuj CALY timer
	T0TCR &=~ mTIMER0_RESET; // wylacz (zaprzestaj) resetu (przestaw bit)
	InitTimer0(); //inicjalizuj timer
}

void WaitOnTimer0Match0(void){
	while(0 == (T0IR & mTIMER0_MR0_INTERRUPT_FLAG)){}; // czekam na ustawienie flagi przerwania w rejestrze IR ( Interrupt Register)
	T0IR = mTIMER0_MR0_INTERRUPT_FLAG; // 1 RESERTUJE !!!  --> kasuje flage przerwania
}





